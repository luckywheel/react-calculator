import React from 'react';
import styles from './styles.module.css';

const Button = (props) => {
  const cls = [
    styles.Button,
    styles[props.type]
  ];
  return (
    <button
      key={props.id}
      className={cls.join(' ')}
      onClick={() => {props.onUserClick(props.id)}}
    >
      { props.children }
    </button>
  )

}

export default Button;
