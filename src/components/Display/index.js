import React from 'react';
import styles from './styles.module.css';

const Display = (props) => {
  return (
    <div className={styles.Display}>
      {props.display}
    </div>
  )
}

export default Display;
