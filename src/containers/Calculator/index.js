import React from 'react';
import styles from './styles.module.css';

import Button from '../../components/Button';
import Display from '../../components/Display';

import { NUMBER } from './utils';
import { functionHandler } from './utils/mathFunc';

export default class Calculator extends React.Component {
  state = {
    display: 0,
    prevNumber: 0,
    activeNumber: 0,
    function: null,
    clearDisplay: false
  }

  onUserClickHandler = (id) => {
    if (typeof(id) === 'number') {
      if (!this.state.display || this.state.clearDisplay) {
        this.setState(prevState => ({
          display: `${id}`,
          activeNumber: `${id}`,
          clearDisplay: false
        }))
        return
      }
      this.setState(prevState => ({
        activeNumber: prevState.activeNumber + `${id}`,
        display: prevState.display + `${id}`
      }))
      return
    } else {
      this.setState({
        activeNumber: 0,
        prevNumber: this.state.activeNumber,
        function: id,
        clearDisplay: true
      })
    }

      if (id === '=') {
        const num1 = Number(this.state.prevNumber);
        const num2 = Number(this.state.activeNumber);
        const func = this.state.function;

        const result = functionHandler(num1, num2, func);
        this.setState({
          display: result,
          prevNumber: 0,
          activeNumber: 0,
          function: null,
          clearDisplay: true
        })
      }

      if (id === 'AC') {
        this.setState({
          display: 0,
          prevNumber: 0,
          activeNumber: 0,
          function: null,
          result: false
        })
      }
      if(id === '%' || id === '+/-' || id === ',') {
        alert('Not realeased in this app version)');
      }

  }

  render() {
    return (
      <div className={styles.Calculator}>
      <Display display={this.state.display} />
        <div className={styles.buttonWrapper}>
        { NUMBER.map((buttonItem, index) => {
          return (
            <Button key={buttonItem.id} id={buttonItem.id} type={buttonItem.style} onUserClick={this.onUserClickHandler}>{buttonItem.id}</Button>
          )
        })}
        </div>
      </div>
    )
  }
}
