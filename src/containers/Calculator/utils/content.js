export const NUMBER = [
  {
    id: 'AC',
    style: 'row'
  },
  {
    id: '+/-',
    style: 'row'
  },
  {
    id: '%',
    style: 'row'
  },
  {
    id: '/',
    style: 'column'
  },
  {
    id: 7,
    style: 'number'
  },
  {
    id: 8,
    style: 'number'
  },
  {
    id: 9,
    style: 'number'
  },
  {
    id: 'X',
    style: 'column'
  },
  {
    id: 4,
    style: 'number'
  },
  {
    id: 5,
    style: 'number'
  },
  {
    id: 6,
    style: 'number'
  },
  {
    id: '-',
    style: 'column'
  },
  {
    id: 1,
    style: 'number'
  },
  {
    id: 2,
    style: 'number'
  },
  {
    id: 3,
    style: 'number'
  },
  {
    id: '+',
    style: 'column'
  },
  {
    id: 0,
    style: 'big-number'
  },
  {
    id: ',',
    style: 'number'
  },
  {
    id: '=',
    style: 'column'
  }
]
