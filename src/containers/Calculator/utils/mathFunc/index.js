import {funcDivision} from './Division';
import {funcMinus} from './Minus';
import {funcMultipull} from './Multipull';
import {funcPlus} from './Plus';
import {funcPercent} from './Percent';
import {funcRevers} from './Revers';

export function functionHandler(num1, num2, func) {
  switch (func) {
    case '+':
      return funcPlus(num1, num2);
    case '-':
      return funcMinus(num1, num2);
    case 'X':
      return funcMultipull(num1, num2);
    case '/':
      return funcDivision(num1, num2);
    case '%':
      return funcPercent(num1, num2);
    case '+/-':
      return funcRevers(num1);
    default:

  }
}
